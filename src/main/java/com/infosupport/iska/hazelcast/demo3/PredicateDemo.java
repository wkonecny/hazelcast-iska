package com.infosupport.iska.hazelcast.demo3;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import com.hazelcast.query.SqlPredicate;
import com.infosupport.iska.hazelcast.model.Employee;

import java.util.Set;

public class PredicateDemo {
    public static void main(String[] args) {
        // Create Hazelcast Instance
        Config cfg = new Config();
        HazelcastInstance instance = Hazelcast.newHazelcastInstance(cfg);

        // Fill map with Employees
        IMap<Integer, Employee> map = instance.getMap("employee");
        map.put(1, new Employee("Jim", 12, true));
        map.put(2, new Employee("Ben", 18, false));
        map.put(3, new Employee("Wouter", 23, true));
        map.put(4, new Employee("Nick", 23, true));
        map.put(5, new Employee("Jeffrey", 31, true));

        // 1. The Hazelcast Predicate -> SQL Style
        sqlPredicateExample(map);

        System.out.println("------------------------------");

        // 2. The Hazelcast Predicate -> JPA Style
        jpaPredicateExample(instance);
    }

    private static void jpaPredicateExample(HazelcastInstance instance) {
        IMap map2 = instance.getMap("employee");
        EntryObject e = new PredicateBuilder().getEntryObject();
        Predicate myPredicate = e.is("active").and(e.get("age").lessThan(30));
        Set<Employee> employeesSet2 = (Set<Employee>) map2.values(myPredicate);

        for (Employee employee : employeesSet2) {
            System.out.println(employee.toString());
        }
    }

    private static void sqlPredicateExample(IMap<Integer, Employee> map) {
        SqlPredicate sqlPredicate = new SqlPredicate("active AND age < 30");
        Set<Employee> employeesSet1 = (Set<Employee>) map.values(sqlPredicate);

        for (Employee employee : employeesSet1) {
            System.out.println(employee.toString());
        }
    }
}
