package com.infosupport.iska.hazelcast.demo1;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class MapDifferences {
    public static void main(String[] args) {
        HazelcastInstance instance = Hazelcast.newHazelcastInstance();

        Map<Integer, String> map = new HashMap<>();
        map.put(1, "Yes");
        map.put(2, "No");

        Map<Integer, String> concurrentMap = new ConcurrentHashMap<>();
        concurrentMap.put(1, "Yes");
        concurrentMap.put(2, "No");

        Map<Integer, String> concurrentDistributedMap = instance.getMap("mapname");
        concurrentDistributedMap.put(1, "Yes");
        concurrentDistributedMap.put(2, "No");
    }
}
