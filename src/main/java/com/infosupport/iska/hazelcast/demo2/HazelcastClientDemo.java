package com.infosupport.iska.hazelcast.demo2;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.infosupport.iska.hazelcast.model.Employee;

import java.util.logging.Logger;

/**
 * This Client will connect to a Hazelcast node in your cluster.
 * It will then read the "employees" map that exists on the Hazelcast Server.
 */
public class HazelcastClientDemo {
    private static Logger logger = Logger.getLogger("HazelcastClientDemo");

    public static void main(String[] args) {
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.addAddress("127.0.0.1:5701");

        HazelcastInstance client = HazelcastClient.newHazelcastClient(clientConfig);

        IMap<Integer, Employee> employeeMap = client.getMap("employees");

        for (Employee employee : employeeMap.values()) {
            logger.info(employee.toString());
        }
    }
}
