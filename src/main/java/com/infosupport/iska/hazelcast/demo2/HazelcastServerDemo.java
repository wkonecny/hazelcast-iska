package com.infosupport.iska.hazelcast.demo2;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.query.*;
import com.infosupport.iska.hazelcast.model.Employee;

import java.util.Map;
import java.util.Set;

/**
 * First start this server to create a cluster of 2 Hazelcast nodes.
 * Next, start the client to read the "employees" map from te server.
 */
public class HazelcastServerDemo {
    public static void main(String[] args) {
        HazelcastInstance instance = Hazelcast.newHazelcastInstance();
        HazelcastInstance instance2 = Hazelcast.newHazelcastInstance();

        Map<Integer,Employee> employees = instance.getMap("employees");
        employees.put(1, new Employee("Jim", 12, true));
        employees.put(2, new Employee("Ben", 18, false));
        employees.put(3, new Employee("Wouter", 23, true));
        employees.put(4, new Employee("Nick", 23, true));
    }
}
