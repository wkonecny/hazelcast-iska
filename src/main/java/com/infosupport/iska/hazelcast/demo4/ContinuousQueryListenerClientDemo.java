package com.infosupport.iska.hazelcast.demo4;


import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.EntryEvent;
import com.hazelcast.core.EntryListener;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.Predicate;
import com.hazelcast.query.PredicateBuilder;
import com.infosupport.iska.hazelcast.model.Employee;

/**
 * First start the ContinuousQueryServer  and then attach this ContinuousQueryListenerClient.
 * When the listener is attached proceed with inserting data using the DataInputClient.
 */
public class ContinuousQueryListenerClientDemo {
    public static void main(String[] args) {

        ClientConfig clientConfig = new ClientConfig();
        HazelcastInstance client = HazelcastClient
                .newHazelcastClient(clientConfig);

        EntryListener<Long, Employee> entryListener = new EntryListener<Long, Employee>() {

            @Override
            public void entryUpdated(EntryEvent<Long, Employee> entry) {
                System.out.println("New employee has been updated with key: " +
                        entry.getKey() + " and value: " + entry.getValue().toString());
            }

            @Override
            public void entryRemoved(EntryEvent<Long, Employee> arg0) {
            }

            @Override
            public void entryEvicted(EntryEvent<Long, Employee> arg0) {
            }

            @Override
            public void entryAdded(EntryEvent<Long, Employee> entry) {
                System.out.println("New employee has been added with key: " +
                        entry.getKey() + " and value: " + entry.getValue().toString());
            }
        };

        EntryObject e = new PredicateBuilder().getEntryObject();
        Predicate predicate = e.is("active").and(e.get("age").lessThan(30));

        IMap<Long, Employee> map = client.getMap("employees");
        map.addEntryListener(entryListener, predicate, true);


    }

}
