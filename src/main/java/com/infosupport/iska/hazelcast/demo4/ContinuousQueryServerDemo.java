package com.infosupport.iska.hazelcast.demo4;

import com.hazelcast.config.Config;
import com.hazelcast.core.Hazelcast;

/**
 * First start this server and then attach the ContinuousQueryListenerClient.
 * When the listener is attached proceed with inserting data using the DataInputClient.
 */
public class ContinuousQueryServerDemo {
    public static void main(String[] args) throws InterruptedException {
        Config config = new Config();
        Hazelcast.newHazelcastInstance(config);
    }
}
