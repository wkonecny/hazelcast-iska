package com.infosupport.iska.hazelcast.demo4;

import com.hazelcast.client.HazelcastClient;
import com.hazelcast.client.config.ClientConfig;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.infosupport.iska.hazelcast.model.Employee;

public class DataInputClientDemo {

    public static void main(String[] args) throws InterruptedException {
        ClientConfig clientConfig = new ClientConfig();
        HazelcastInstance client = HazelcastClient
                .newHazelcastClient(clientConfig);

        IMap<Long, Employee> map = client.getMap("employees");

        for (long i = 0; i < 60; i++) {
            int age = (int) Math.round(Math.random() * 90);
            map.put(i,
                    new Employee("Harry" + i, age
                            , true));
            System.out.println("Added employee: Harry" + i + " age =" + age);
            Thread.sleep(100);
        }

        IMap<Long, Employee> mapSize = client.getMap("employees");
        System.out.println("size " + mapSize.size());
    }

}
