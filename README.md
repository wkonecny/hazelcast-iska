# Hazelcast ISKA Code Examples
![Alt text](http://s3.amazonaws.com/crunchbase_prod_assets/assets/images/resized/0012/2424/122424v4-max-250x250.png)

Hazelcast is an open source in-memory data grid it provides Java developers an easy-to-use and powerful solution for data distribution that can be used as a clustering solution, a distributed cache and a NoSQL key-value store
